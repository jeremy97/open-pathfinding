﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;
using TriangleNet;
using TriangleNet.Geometry;

using Path = System.Collections.Generic.List<ClipperLib.IntPoint>;
using Paths = System.Collections.Generic.List<System.Collections.Generic.List<ClipperLib.IntPoint>>;

namespace OpenPathfinding
{
    public class NavMeshGenerator : MonoBehaviour
    {
        public bool drawGizmos = false;
        public bool drawPaths = false;
        public bool drawMeshes = false;
        [SerializeField, Range(0f, 89f)]
        private float m_maximumWalkableAngle = 0f;
        [SerializeField]
        private float m_characterRadius = 0f;
        [SerializeField, Range(1, 10000)]
        private float m_accuracyMultiplier;

        private List<List<Polygon>> decompMeshes;

        private void Awake()
        {
            GenerateNavMeshes();
        }

        /// <summary>
        /// Generates nav meshes by feeding mesh data into Clipper
        /// </summary>
        private void GenerateNavMeshes()
        {
            GameObject[] walkableObjects = GameObject.FindGameObjectsWithTag("Walkable");
            Paths solution = ClipPolygons(walkableObjects);

            //Draw generated clipped paths in scene
            if(drawPaths)
                DrawPaths(ref solution);

            //Triangulate the clipped paths
            List<UnityEngine.Mesh> triangulatedMeshes = TriangulatePaths(ref solution);

            //Draw the triangulated meshes
            if(drawMeshes)
                DrawMeshes(ref triangulatedMeshes);

            //Run Hertel-Mehlhorn on the triangulated meshes to get simplified convex polygons for our graph data
            //List<List<Polygon>> decomposedMeshes = Decompose(triangulatedMeshes);
            decompMeshes = Decompose(triangulatedMeshes);

            GetComponent<Graph>().Setup(decompMeshes[0]);
        }

        private void OnDrawGizmos()
        {
            if (drawGizmos && decompMeshes != null)
            {
                for (int i = 0; i < decompMeshes.Count; i++)
                {
                    for (int j = 0; j < decompMeshes[i].Count; j++)
                    {
                        for (int k = 0; k < decompMeshes[i][j].vertices.Count; k++)
                        {
                            Gizmos.color = new Color(1f, 0f, 0f, 1f);
                            Gizmos.DrawSphere(decompMeshes[i][j].vertices[k].position, 0.015f);
                            Gizmos.DrawLine(decompMeshes[i][j].vertices[k].position, decompMeshes[i][j].vertices[k].halfEdge.vertex.position);
                        }
                    }
                }
            }
        }

        private void DrawDecomposedMeshes(ref List<List<Polygon>> aDecomposedMeshes)
        {
            for(int i = 0; i < aDecomposedMeshes.Count; i++)
            {
                for(int j = 0; j < aDecomposedMeshes[i].Count; j++)
                {
                    GameObject go = new GameObject();
                    LineRenderer lr = go.AddComponent<LineRenderer>();
                    lr.loop = true;
                    lr.startWidth = 0.03f;
                    lr.endWidth = 0.03f;
                    lr.positionCount = aDecomposedMeshes[i][j].vertices.Count;

                    for (int k = 0; k < aDecomposedMeshes[i][j].vertices.Count; k++)
                    {
                        lr.SetPosition(k, aDecomposedMeshes[i][j].vertices[k].position);
                    }
                }
            }
        }

        /// <summary>
        /// Performs a convex decomposition of triangulated meshes using Hertel-Mehlhorn algorithm
        /// </summary>
        /// <param name="aMeshes">Meshes to decompose</param>
        /// <returns>List of meshes, made up of lists of the polygons that make up each mesh</returns>
        private List<List<Polygon>> Decompose(List<UnityEngine.Mesh> aMeshes)
        {
            List<List<Polygon>> meshes = new List<List<Polygon>>(aMeshes.Count);

            foreach(UnityEngine.Mesh mesh in aMeshes)
            {
                List<HalfEdge> decomposedHalfEdges = GeomUtil.DecomposeConvex(mesh.triangles, mesh.vertices);
                meshes.Add(GeomUtil.HalfEdgesToPolygons(decomposedHalfEdges));
            }

            return meshes;
        }

        /// <summary>
        /// Generates clipped polygon(s) from meshes on the given GameObjects.
        /// </summary>
        /// <param name="aWalkableObjects">GameObjects which are tagged as walkable and should have meshes associated with them.</param>
        /// <returns>Paths defining clipped polygons.</returns>
        private Paths ClipPolygons(GameObject[] aWalkableObjects)
        {
            Paths solution = new Paths();
            Paths subjects = new Paths();

            //Go through every GameObject tagged as Walkable
            foreach (GameObject go in aWalkableObjects)
            {
                subjects.Add(GeneratePathFromMesh(go));
            }

            //Expand the paths slightly to ensure that adjacent edges are treated as overlapping
            ClipperOffset expandPaths = new ClipperOffset();
            expandPaths.AddPaths(subjects, JoinType.jtMiter, EndType.etClosedPolygon);
            expandPaths.Execute(ref solution, 300.0f); //expand each poly outwards by 1/10th of the accuracy multiplier

            //Intersect operation on all of the collected polys
            Clipper c = new Clipper();
            c.AddPaths(solution, PolyType.ptClip, true);
            c.AddPaths(solution, PolyType.ptSubject, true);
            c.Execute(ClipType.ctUnion, solution, PolyFillType.pftEvenOdd, PolyFillType.pftNonZero);

            //Contract the combined polys using character radius to ensure navmeshes are not right up to the edge
            ClipperOffset contract = new ClipperOffset();
            contract.AddPaths(solution, JoinType.jtMiter, EndType.etClosedPolygon);
            contract.Execute(ref solution, -m_characterRadius * m_accuracyMultiplier - 300.0f);

            return solution;
        }

        private void DrawMeshes(ref List<UnityEngine.Mesh> meshes)
        {
            foreach(UnityEngine.Mesh mesh in meshes)
            {
                GameObject go = new GameObject("Triangulated Mesh");
                MeshFilter mf = go.AddComponent<MeshFilter>();
                MeshRenderer mr = go.AddComponent<MeshRenderer>();

                mf.mesh = mesh;
            }
        }

        /// <summary>
        /// Draws clipped paths in the scene using LineRenderers.
        /// </summary>
        /// <param name="aPaths">The paths of the polygons to be drawn.</param>
        private void DrawPaths(ref Paths aPaths)
        {
            Debug.Log("Solution count " + aPaths.Count);

            for (int i = 0; i < aPaths.Count; i++)
            {
                GameObject go = new GameObject("Paths");
                LineRenderer lr = go.AddComponent<LineRenderer>();
                lr.loop = true;
                lr.startWidth = 0.03f;
                lr.endWidth = 0.03f;
                lr.positionCount = aPaths[i].Count;

                for (int j = 0; j < aPaths[i].Count; j++)
                {
                    lr.SetPosition(j, new Vector3(aPaths[i][j].X / m_accuracyMultiplier, 0.2f, aPaths[i][j].Y / m_accuracyMultiplier));
                }
            }
        }

        /// <summary>
        /// Gets valid triangles on the given GameObject's mesh and generates paths usable by Clipper.
        /// </summary>
        /// <param name="aGameObject">The GameObject being operated on.</param>
        /// <returns>The Clipper Path for the faces of the GameObject's mesh that are within the maximum walkable angle.</returns>
        private Path GeneratePathFromMesh(GameObject aGameObject)
        {
            //Make sure the GameObject has a MeshFilter
            MeshFilter mf = aGameObject.GetComponent<MeshFilter>();

            //Skip the GameObject if it doesn't have a MeshFilter or the MeshFilter has no mesh
            if (mf == null)
            {
                Debug.LogWarning("GameObject '" + aGameObject.name + "' was tagged as walkable but it has no MeshFilter attached! It will not be added to the navmesh.");
                return null;
            }
            else if (mf.sharedMesh == null)
            {
                Debug.LogWarning(aGameObject.name + " has no mesh in its MeshFilter! It will not be added to the navmesh.");
                return null;
            }
            
            UnityEngine.Mesh mesh = mf.sharedMesh;
            Path path = new Path();

            //Go through this GameObject's mesh's triangles to determine which are within the walkable angle from the up direction
            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                //Get the world positions for each of the triangle's vertices
                Vector3 v0 = aGameObject.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 0]]);
                Vector3 v1 = aGameObject.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 1]]);
                Vector3 v2 = aGameObject.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 2]]);

                //Calculate the center point of the triangle
                Vector3 triCenter = (v0 + v1 + v2) / 3f;

                //Use cross product to calculate the normal of the triangle's face
                Vector3 triSide1 = v1 - v0;
                Vector3 triSide2 = v2 - v0;
                Vector3 faceNormal = Vector3.Cross(triSide1, triSide2);
                faceNormal.Normalize();

                //Get the angle between the triangle's normal and the up direction
                float angle = MathUtil.AngleBetween(Vector3.up, faceNormal);

                //The triangle's normal is within the walkable angle. Add to navmesh
                if (angle <= m_maximumWalkableAngle)
                {
                    //Debug.DrawRay(triCenter, faceNormal / 5, Color.magenta, 20f);
                    IntPoint p0 = new IntPoint((long)(v0.x * m_accuracyMultiplier), (long)(v0.z * m_accuracyMultiplier));
                    IntPoint p1 = new IntPoint((long)(v1.x * m_accuracyMultiplier), (long)(v1.z * m_accuracyMultiplier));
                    IntPoint p2 = new IntPoint((long)(v2.x * m_accuracyMultiplier), (long)(v2.z * m_accuracyMultiplier));

                    path.Add(p0);
                    path.Add(p1);
                    path.Add(p2);
                    path.Add(p0);
                }
            }
            return path;
        }

        /// <summary>
        /// Triangulates every Path in a Clipper Paths object
        /// </summary>
        /// <param name="aPaths">The paths to triangulate</param>
        /// <returns>Delaunay-triangulated meshes</returns>
        private List<UnityEngine.Mesh> TriangulatePaths(ref Paths aPaths)
        {
            List<UnityEngine.Mesh> triangulated = new List<UnityEngine.Mesh>(aPaths.Count);

            for (int i = 0; i < aPaths.Count; i++)
            {
                triangulated.Add(Triangulate(aPaths[i]));
            }

            return triangulated;
        }

        /// <summary>
        /// Triangulates a single Clipper Path using Triangle.NET Delaunay triangulation
        /// </summary>
        /// <param name="path">The Clipper Path containing vertices to be triangulated.</param>
        /// <returns>The triangulated mesh</returns>
        private UnityEngine.Mesh Triangulate(Path path)
        {
            //Fill Triangle.NET's Polygon structure with the vertices of the path
            TriangleNet.Geometry.Polygon polygon = new TriangleNet.Geometry.Polygon(path.Count);
            for (int i = 0; i < path.Count; i++)
            {
                polygon.Add(new TriangleNet.Geometry.Vertex(path[i].X, path[i].Y));

                if(i == path.Count - 1)
                {
                    polygon.Add(new Segment(new TriangleNet.Geometry.Vertex(path[i].X, path[i].Y), new TriangleNet.Geometry.Vertex(path[0].X, path[1].Y)));
                }
                else
                {
                    polygon.Add(new Segment(new TriangleNet.Geometry.Vertex(path[i].X, path[i].Y), new TriangleNet.Geometry.Vertex(path[i+1].X, path[i+1].Y)));
                }
            }

            //Constrain the triangulation to conforming delaunay. If false, we get long, thin triangles at edges
            //because by default the algorithm tries to keep the mesh convex.
            TriangleNet.Meshing.ConstraintOptions options = new TriangleNet.Meshing.ConstraintOptions();
            options.ConformingDelaunay = true;
            options.Convex = false;
            options.SegmentSplitting = 1;
            //Triangulate using the ConstraintOptions
            TriangleNet.Mesh triangulatedMesh = (TriangleNet.Mesh)polygon.Triangulate(options);

            //Data to fill the new UnityEngine.Mesh we're returning
            List<Vector3> vertices = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> uvs = new List<Vector2>();
            List<int> triangles = new List<int>();

            //Triangle.NET doesn't give us any array-like interface for indexing. Use an enumerator to go through them.
            IEnumerator<TriangleNet.Topology.Triangle> triangleEnumerator = triangulatedMesh.triangles.GetEnumerator();
            
            //for (triangleEnumerator = triangulatedMesh.triangles.GetEnumerator(); triangleEnumerator.Current != null; triangleEnumerator.MoveNext())
            for (int i = 0; i < triangulatedMesh.triangles.Count; i++)
            {
                //If we hit the last triangle, we're done
                if (!triangleEnumerator.MoveNext())
                {
                    break;
                }

                //Grab the current triangle from the enumerator
                TriangleNet.Topology.Triangle triangle = triangleEnumerator.Current;

                //Triangle.NET gives us triangles defined in the opposite order from what Unity's Mesh wants. Feed them in reversed.
                Vector3 v0 = GetVertexPositionAtIndex(ref triangulatedMesh, triangulatedMesh.vertices[triangle.vertices[2].id].id);
                Vector3 v1 = GetVertexPositionAtIndex(ref triangulatedMesh, triangulatedMesh.vertices[triangle.vertices[1].id].id);
                Vector3 v2 = GetVertexPositionAtIndex(ref triangulatedMesh, triangulatedMesh.vertices[triangle.vertices[0].id].id);

                //This triangle is made of the next 3 vertices we add
                triangles.Add(vertices.Count);
                triangles.Add(vertices.Count + 1);
                triangles.Add(vertices.Count + 2);

                //Add the vertices
                vertices.Add(v0);
                vertices.Add(v1);
                vertices.Add(v2);

                //Add normals because Unity's Mesh wants them
                normals.Add(Vector3.up);
                normals.Add(Vector3.up);
                normals.Add(Vector3.up);

                //Add uvs because Unity's Mesh wants them
                uvs.Add(Vector2.zero);
                uvs.Add(Vector2.zero);
                uvs.Add(Vector2.zero);
            }

            //Mesh to be returned. Fill with data we just grabbed from the triangulatedMesh
            UnityEngine.Mesh finalMesh = new UnityEngine.Mesh();
            finalMesh.vertices = vertices.ToArray();
            finalMesh.normals = normals.ToArray();
            finalMesh.uv = uvs.ToArray();
            finalMesh.triangles = triangles.ToArray();

            return finalMesh;
        }

        /// <summary>
        /// Grabs vertex data from the given TriangleNet.Mesh at the given index and converts it to a Vector3.
        /// </summary>
        /// <param name="mesh">The mesh to grab the data from.</param>
        /// <param name="index">The index at which to grab the data.</param>
        /// <returns>The mesh vertex as a Vector3</returns>
        private Vector3 GetVertexPositionAtIndex(ref TriangleNet.Mesh mesh, int index)
        {
            TriangleNet.Geometry.Vertex vertex = mesh.vertices[index];
            return new Vector3((float)vertex.x / m_accuracyMultiplier, .1f, (float)vertex.y / m_accuracyMultiplier);
        }
    }
}
