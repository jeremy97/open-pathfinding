﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public class HalfEdge
    {
        //The vertex that this edge points to
        public Vertex vertex;

        //The polygon this edge is part of
        public Polygon polygon;

        public HalfEdge next;
        public HalfEdge previous;
        public HalfEdge opposite;

        public HalfEdge(Vertex aVertex)
        {
            vertex = aVertex;
        }
    }
}