﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public static class GeomUtil
    {
        /// <summary>
        /// Converts a list of <see cref="Polygon"/> to their constituent HalfEdges with all internal references correctly filled out.
        /// </summary>
        /// <param name="aPolygons">The Polygons to grab the HalfEdges from.</param>
        /// <returns>The <see cref="HalfEdge"/>s which make up the given <see cref="Polygon"/>s</returns>
        public static List<HalfEdge> PolygonsToHalfEdges(ref List<Polygon> aPolygons)
        {
            List<HalfEdge> halfEdges = new List<HalfEdge>(aPolygons.Count * 3);

            //build out halfedges for each poly
            for(int i = 0; i < aPolygons.Count; i++)
            {
                List<HalfEdge> currHalfEdges = new List<HalfEdge>(aPolygons[i].vertices.Count);

                //create the halfedges
                for(int j = 0; j < aPolygons[i].vertices.Count; j++)
                {
                    currHalfEdges.Add(new HalfEdge(aPolygons[i].vertices[j]));
                }
                //fill out the data in the halfedges
                for(int j = 0; j < currHalfEdges.Count; j++)
                {
                    //special case: first HalfEdge, loop around to end
                    if(j == 0)
                    {
                        currHalfEdges[j].next = currHalfEdges[j + 1];
                        currHalfEdges[j].previous = currHalfEdges[currHalfEdges.Count - 1];
                    }
                    //special case: last HalfEdge, loop around to beginning
                    else if(j == currHalfEdges.Count - 1)
                    {
                        currHalfEdges[j].next = currHalfEdges[0];
                        currHalfEdges[j].previous = currHalfEdges[j - 1];
                    }
                    else
                    {
                        currHalfEdges[j].next = currHalfEdges[j + 1];
                        currHalfEdges[j].previous = currHalfEdges[j - 1];
                    }

                    currHalfEdges[j].vertex.halfEdge = currHalfEdges[j].next;
                    currHalfEdges[j].polygon = aPolygons[i];
                    currHalfEdges[j].vertex.polygon = aPolygons[i];
                }

                aPolygons[i].halfEdge = currHalfEdges[0];
                halfEdges.AddRange(currHalfEdges);
            }

            //Link up HalfEdges that are opposite each other
            for (int i = 0; i < halfEdges.Count; i++)
            {
                HalfEdge he = halfEdges[i];

                Vertex goingToVertex = he.vertex;
                Vertex goingFromVertex = he.previous.vertex;

                for (int j = 0; j < halfEdges.Count; j++)
                {
                    //Don't compare with self
                    if (i == j)
                    {
                        continue;
                    }

                    HalfEdge heOpposite = halfEdges[j];

                    //Is this edge going between the vertices in the opposite direction
                    if (goingFromVertex.position == heOpposite.vertex.position
                        && goingToVertex.position == heOpposite.previous.vertex.position)
                    {
                        he.opposite = heOpposite;
                        break;
                    }
                }
            }

            return halfEdges;
        }

        /*
         * Hertel-Mehlhorn Resources *
         * http://kaba.hilvi.org/homepage/blog/halfedge/halfedge.htm
         * https://stackoverflow.com/questions/49847136/how-to-remove-an-edge-from-a-half-edge-structure
         * http://crtl-i.com/PDF/comp_c.pdf
         * https://web.archive.org/web/20140203030933/http://www.philvaz.com/compgeom/ <- "Hertel-Mehlhorn Algorithm"
         * http://www.dma.fi.upm.es/recursos/aplicaciones/geometria_computacional_y_grafos/web/piezas_convexas/algoritmo_i.html
         */


        /// <summary>
        /// Hertel-Mehlhorn implementation, removing non-integral edges from the given mesh.
        /// </summary>
        /// <param name="aTriangles"><see cref="UnityEngine.Mesh"/> data defining triangles of the mesh based on indices into aVertices</param>
        /// <param name="aVertices"><see cref="UnityEngine.Mesh"/> data defining vertices of the mesh</param>
        /// <returns>Convex <see cref="Polygon"/>s making up the entire mesh</returns>
        public static List<HalfEdge> DecomposeConvex(int[] aTriangles, Vector3[] aVertices)
        {
            //convert the unity mesh data to Polygons
            List<Polygon> polys = new List<Polygon>(aTriangles.Length);

            for(int i = 0; i < aTriangles.Length; i += 3)
            {
                List<Vector3> positions = new List<Vector3>(3) {
                    aVertices[aTriangles[i + 0]],
                    aVertices[aTriangles[i + 1]],
                    aVertices[aTriangles[i + 2]]
                };

                polys.Add(new Polygon(positions));
            }

            //Convert the Polygons to HalfEdges
            List<HalfEdge> halfEdges = PolygonsToHalfEdges(ref polys);

            //Hertel-Mehlhorn algorithm on the HalfEdges
            for(int i = 0; i < halfEdges.Count; i++)
            {
                //If there's no opposite HalfEdge, we're on the outside edge of the mesh so
                //this edge shouldn't be considered for deletion as it's integral to the mesh's shape
                if (halfEdges[i].opposite != null)
                {
                    if (ShouldRemoveEdge(halfEdges[i]))
                    {
                        //The edge should be removed. This means we need to remove both HalfEdges which make up the edge and we need
                        //to update references such that there is only a single merged Polygon remaining after the dust settles.

                        //The 4 HalfEdges adjacent to the two removed need refs updated for .next/.previous
                        //(2 need .next updated, 2 need .previous updated)
                        halfEdges[i].previous.next = halfEdges[i].opposite.next;
                        halfEdges[i].opposite.previous.next = halfEdges[i].next;
                        halfEdges[i].next.previous = halfEdges[i].opposite.previous;
                        halfEdges[i].opposite.next.previous = halfEdges[i].previous;

                        //Also need to clean up the two sets of overlapping vertices
                        halfEdges[i].previous.vertex = halfEdges[i].opposite.vertex;
                        halfEdges[i].opposite.previous.vertex = halfEdges[i].vertex;
                        
                        //Make sure the HalfEdge we're deleting isn't the one that the poly has reference to
                        halfEdges[i].polygon.halfEdge = halfEdges[i].previous;

                        //The first vertex in the loop of new vertices needs the correct polygon, also add it to the merged polygon
                        halfEdges[i].previous.vertex.polygon = halfEdges[i].polygon;
                        halfEdges[i].polygon.vertices.Add(halfEdges[i].previous.vertex);

                        //One polygon must be deleted. The .polygon refs for the HalfEdges and vertices that
                        //were on that poly must be updated to the one remaining poly.
                        HalfEdge curr = halfEdges[i].previous.next;
                        Polygon removedPoly = curr.polygon;

                        //iterate through all HalfEdges in the Polygon we're removing except the last
                        while(curr != halfEdges[i].next.previous)
                        {

                            curr.polygon = halfEdges[i].polygon;
                            curr.vertex.polygon = halfEdges[i].polygon;
                            halfEdges[i].polygon.vertices.Add(curr.vertex);
                            curr = curr.next;
                        }

                        //Take care of the final case we skipped in the loop because the vertex was already handled
                        curr.polygon = halfEdges[i].polygon;

                        //Clear the removed polygon's vertices
                        removedPoly.vertices.Clear();

                        //Redo the remaining polygon's vertices with the new loop of HalfEdges
                        Polygon remainingPoly = halfEdges[i].polygon;
                        remainingPoly.vertices.Clear();
                        //reset the iterator
                        curr = halfEdges[i].previous.next;

                        while(curr != halfEdges[i].previous)
                        {
                            remainingPoly.vertices.Add(curr.vertex);
                            curr = curr.next;
                        }

                        //Add the last vert
                        remainingPoly.vertices.Add(curr.vertex);

                        //All references to these two HalfEdges are now cleaned up, remove them from the list
                        int oppIndex = halfEdges.IndexOf(halfEdges[i].opposite);
                        halfEdges.RemoveAt(oppIndex);

                        if (oppIndex < i)
                        {
                            i--;
                        }

                        halfEdges.RemoveAt(i);
                        i--;
                    }
                }
            }

            return halfEdges;
        }

        /// <summary>
        /// Converts a list of <see cref="HalfEdge"/>s to a list of the <see cref="Polygon"/>s they're part of
        /// </summary>
        /// <param name="aHalfEdges">The HalfEdges which make up the Polygons</param>
        /// <returns>The Polygons made up by the given HalfEdges</returns>
        public static List<Polygon> HalfEdgesToPolygons(List<HalfEdge> aHalfEdges)
        {
            List<Polygon> polys = new List<Polygon>(aHalfEdges.Count * 2);

            foreach(HalfEdge he in aHalfEdges)
            {
                if (!polys.Contains(he.polygon))
                {
                    polys.Add(he.polygon);
                }
            }

            return polys;
        }

        /// <summary>
        /// Checks the two new angles that would result if the edge which the given <see cref="HalfEdge"/> is a part of
        /// were to be removed to see if they would be less than 180 degrees.
        /// </summary>
        /// <param name="aHalfEdge">The edge to evaluate</param>
        /// <returns>If the given edge should be removed from the mesh.</returns>
        private static bool ShouldRemoveEdge(HalfEdge aHalfEdge)
        {
            HalfEdge curr = aHalfEdge;
            HalfEdge opp = aHalfEdge.opposite;

            //check what new angle would be on first corner if this side were removed

            //create vector for other poly's edge
            Vector3 oppPrevVert1 = opp.previous.vertex.position;
            Vector3 oppPrevVert2 = opp.previous.previous.vertex.position;

            //create vector for this poly's edge
            Vector3 currVert1 = curr.vertex.position;
            Vector3 currVert2 = curr.next.vertex.position;

            //calc the ccw angle between the vectors of the two edges
            float angle1 = MathUtil.SignedAngleBetween((currVert2 - currVert1).normalized, (oppPrevVert2 - oppPrevVert1).normalized, Vector3.up);

            //can't remove this edge if the resulting angle would be >= 180 degrees
            if(angle1 < 0f)
            {
                return false;
            }

            //check what new angle would be on the other corner if this side were removed

            //create vector for other poly's edge
            Vector3 oppVert1 = opp.vertex.position;
            Vector3 oppVert2 = opp.next.vertex.position;

            //create vector for this poly's edge
            Vector3 currPrevVert1 = curr.previous.vertex.position;
            Vector3 currPrevVert2 = curr.previous.previous.vertex.position;

            //calc ccw angle
            float angle2 = MathUtil.SignedAngleBetween((oppVert2 - oppVert1).normalized, (currPrevVert2 - currPrevVert1).normalized, Vector3.up);

            //can't remove if angle >= 180 degrees
            if(angle2 < 0f)
            {
                return false;
            }
            
            //passed both tests, it's nonessential and should be removed
            return true;
        }

        /// <summary>
        /// Determinant-based check of whether a point is within a given <see cref="Polygon"/>
        /// </summary>
        /// <param name="aPolygon">The polygon</param>
        /// <param name="aPoint">The point</param>
        /// <returns>Whether the given point is within the given <see cref="Polygon"/></returns>
        public static bool IsPointWithinPolygon(Polygon aPolygon, Vector3 aPoint)
        {
            HalfEdge he = aPolygon.halfEdge;

            do
            {
                float determinant = CalculateDeterminant(he.previous.vertex, he.vertex, aPoint);
                if (determinant > 0)
                {
                    return false;
                }
                he = he.next;
            }
            while (he != aPolygon.halfEdge);

            return true;
        }

        /// <summary>
        /// Used by <see cref="IsPointWithinPolygon(Polygon, Vector3)"/> to calculate determinants shorthand
        /// </summary>
        /// <param name="aVertexA"></param>
        /// <param name="aVertexB"></param>
        /// <param name="aPoint"></param>
        /// <returns>The determinant of the given points</returns>
        private static float CalculateDeterminant(Vertex aVertexA, Vertex aVertexB, Vector3 aPoint)
        {
            float det = (aVertexB.position.x - aVertexA.position.x) * (aPoint.z - aVertexA.position.z) - (aPoint.x - aVertexA.position.x) * (aVertexB.position.z - aVertexA.position.z);
            return det;
        }

        public static Vector3 GetCenterOfPolygon(Polygon aPolygon)
        {
            Vector3 center = Vector3.zero;

            foreach(Vertex v in aPolygon.vertices)
            {
                center += v.position;
            }

            center /= aPolygon.vertices.Count;
            return center;
        }

        public static Vector3 GetMidpointOfHalfEdge(HalfEdge aHalfEdge)
        {
            Vector3 midpoint = (aHalfEdge.previous.vertex.position + aHalfEdge.vertex.position) / 2;
            return midpoint;
        }
    }
}