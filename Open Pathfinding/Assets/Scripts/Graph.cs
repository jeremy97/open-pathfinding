﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace OpenPathfinding
{
    public class Graph : MonoBehaviour
    {
        public delegate void OnPathingComplete(LinkedList<Line> aPath);

        struct PathRequest
        {
            public Vector3 startPoint;
            public Vector3 endPoint;
            public OnPathingComplete completionCallback;
        }

        public static Graph instance;

        private List<Polygon> nodes;

        private Thread[] m_pathThreads;
        private PriorityQueue<PathRequest> m_pathRequests;

        private void Awake()
        {
            if(instance == null)
            {
                instance = this;

                m_pathRequests = new PriorityQueue<PathRequest>();
                m_pathThreads = new Thread[System.Environment.ProcessorCount];

                for(int i = 0; i < m_pathThreads.Length; i++)
                {
                    m_pathThreads[i] = new Thread(() => { PathThread(); });
                    m_pathThreads[i].Start();
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void Setup(List<Polygon> aNodes)
        {
            nodes = aNodes;
        }

        public void RequestPath(Vector3 aStartPoint, Vector3 aEndPoint, OnPathingComplete aCallback, int aPriority = 0)
        {
            lock (m_pathRequests)
            {
                m_pathRequests.Enqueue(
                    new PathRequest()
                    {
                        startPoint = aStartPoint,
                        endPoint = aEndPoint,
                        completionCallback = aCallback
                    },
                    aPriority);
            }
        }

        private void PathThread()
        {
            PathRequest pr = new PathRequest();
            bool worked = false;
            while (true)
            {
                lock (m_pathRequests) {
                    if (m_pathRequests.Count > 0)
                    {
                        worked = true;
                        pr = m_pathRequests.Dequeue();
                    }
                }

                if (worked)
                {
                    pr.completionCallback(FindPathHeuristic(pr.startPoint, pr.endPoint));
                    worked = false;
                }
                else
                {
                    Thread.Sleep(5);
                }
            }
        }

        /// <summary>
        /// Distance-based heuristic pathfinding algorithm
        /// </summary>
        /// <param name="aStart"></param>
        /// <param name="aEnd"></param>
        /// <returns>Null if no path found, else the path</returns>
        public LinkedList<Line> FindPathHeuristic(Vector3 aStart, Vector3 aEnd)
        {

            Polygon startPoly = GetPolygonAtPosition(aStart);
            if (startPoly == null)
                return null;

            Polygon endPoly = GetPolygonAtPosition(aEnd);
            if(endPoly == null)
                return null;

            //target position is on the same poly we're currently on
            if(startPoly == endPoly)
            {
                LinkedList<Line> result = new LinkedList<Line>();
                result.AddFirst(new Line(aEnd, aEnd));
                return result;
            }

            PriorityQueue<Polygon> frontier = new PriorityQueue<Polygon>();
            frontier.Enqueue(startPoly, Cost(aStart, aEnd));

            Dictionary<Polygon, Polygon> cameFrom = new Dictionary<Polygon, Polygon>();
            cameFrom.Add(startPoly, null);

            while(frontier.Count != 0)
            {
                Polygon current = frontier.Dequeue();

                //early out
                if (current == endPoly)
                    break;

                HalfEdge he = current.halfEdge;

                do
                {
                    if (he.opposite != null && !cameFrom.ContainsKey(he.opposite.polygon))
                    {
                        frontier.Enqueue(he.opposite.polygon, Cost(GeomUtil.GetMidpointOfHalfEdge(he), aEnd));
                        cameFrom.Add(he.opposite.polygon, current);
                    }

                    he = he.next;
                }
                while (he != current.halfEdge);
            }

            //end wasn't found, we can't get there
            if (!cameFrom.ContainsKey(endPoly))
                return null;

            LinkedList<Line> path = new LinkedList<Line>();

            //add endpoint
            path.AddFirst(new Line(aEnd, aEnd));

            Polygon tracker = endPoly;
            
            while(tracker != startPoly)
            {
                path.AddFirst(GetConnectingLine(tracker, cameFrom[tracker]));
                tracker = cameFrom[tracker];
            }
            return path;
        }

        /// <summary>
        /// Heuristic cost function to traverse between two points
        /// </summary>
        /// <param name="aPolygonA"></param>
        /// <param name="aPolygonB"></param>
        /// <returns>The "cost" to travel between two polygons</returns>
        private int Cost(Vector3 aStart, Vector3 aEnd)
        {
            return (int)(aStart - aEnd).sqrMagnitude;
        }

        private Polygon GetPolygonAtPosition(Vector3 aPosition)
        {
            foreach(Polygon p in nodes)
            {
                if(GeomUtil.IsPointWithinPolygon(p, aPosition))
                {
                    return p;
                }
            }
            return null;
        }

        private Vector3 GetConnectingPoint(Polygon aPolygonA, Polygon aPolygonB)
        {
            HalfEdge he = aPolygonA.halfEdge;
            int count = 0;

            do
            {
                if(he.opposite != null && he.opposite.polygon == aPolygonB)
                {
                    break;
                }
                he = he.next;
                count++;
            }
            while (he != aPolygonA.halfEdge);

            if (he != aPolygonA.halfEdge || count == 0)
                return GeomUtil.GetMidpointOfHalfEdge(he);
            else
                return new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        }

        private Line GetConnectingLine(Polygon aPolygonA, Polygon aPolygonB)
        {
            HalfEdge he = aPolygonA.halfEdge;
            int count = 0;

            do
            {
                if (he.opposite != null && he.opposite.polygon == aPolygonB)
                {
                    break;
                }
                he = he.next;
                count++;
            }
            while (he != aPolygonA.halfEdge);

            if (he != aPolygonA.halfEdge || count == 0)
                return new Line(he.vertex.position, he.previous.vertex.position);
            else
                return null;
        }
    }
}