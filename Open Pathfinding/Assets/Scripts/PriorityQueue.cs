﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<DataType>
{
    class PQElement
    {
        public DataType data;
        public int priority;
    }

    LinkedList<PQElement> linkedList = new LinkedList<PQElement>();

    public int Count {
        get {
            return linkedList.Count;
        }
    }

    public void Enqueue(DataType aData, int aPriority)
    {
        //create new element from the passed in data
        PQElement newElement = new PQElement()
        {
            data = aData,
            priority = aPriority
        };

        //grab reference to first element, this variable will act as our iterator
        LinkedListNode<PQElement> curr = linkedList.First;

        //loop to the end
        while (curr != null)
        {
            //the new node's priority is less than the current node's priority, add it before the current node. we're done here
            if (aPriority <= curr.Value.priority)
            {
                linkedList.AddBefore(curr, newElement);
                return;
            }
            //otherwise move onto the next node
            else
            {
                curr = curr.Next;
            }
        }

        //if we made it here, the new node's priority is greater than any other node's in the list. add it to the end
        linkedList.AddLast(newElement);
    }

    public DataType Dequeue()
    {
        PQElement firstElement = linkedList.First.Value;
        linkedList.RemoveFirst();
        return firstElement.data;
    }
}