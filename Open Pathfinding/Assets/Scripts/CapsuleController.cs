﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public enum PathType
    {
        Shortest,
        Midpoint,
        Smoothed
    }

    public class CapsuleController : MonoBehaviour
    {
        public float speed = 2f;
        public float minDistanceFromCurrentTarget = 0.05f;
        public PathType pathType = PathType.Midpoint;
        NavAgent agent;

        private void Start()
        {
            agent = GetComponent<NavAgent>();
            GetComponent<MeshRenderer>().material.color = Random.ColorHSV(0, 1, 0, 1, 0, 1, 1, 1);

            speed += Random.Range(-speed / 2, speed / 2);
        }

        void CheckInput()
        {
            //if left click, do raycast
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                //do raycast
                if (Physics.Raycast(ray, out hit, 100f))
                {
                    //grab endpoint from click position
                    Vector3 end = hit.point;

                    //if it's not null then get the path to it
                    if (end != null)
                    {
                        agent.FindPathTo(end);
                    }
                }
            }
        }

        private void Update()
        {
            //if we have a path, then let's follow it
            if (agent.CurrentTarget != null)
            {
                //get the next node in the path
                Vector3 target = GetTargetPosition(agent.CurrentTarget, pathType) + Vector3.up;

                //calculate movement vector
                Vector3 moveVector = target - transform.position;
                moveVector.Normalize();
                moveVector *= speed * Time.deltaTime;

                //move
                transform.position += moveVector;

                //check if we're close enough to target that we can move onto the next node
                if ((transform.position - target).sqrMagnitude < minDistanceFromCurrentTarget * minDistanceFromCurrentTarget)
                {
                    agent.UpdateTarget();
                }
            }
        }

        private void LateUpdate()
        {
            CheckInput();
        }

        Vector3 GetTargetPosition(Line aTargetLine, PathType aPathType)
        {
            switch (aPathType)
            {
                case PathType.Shortest:
                case PathType.Smoothed:
                case PathType.Midpoint:
                default:
                    return (aTargetLine.pointA + aTargetLine.pointB) / 2;
            }
        }
    }
}