﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public class Edge
    {
        public Vertex vertex1;
        public Vertex vertex2;

        public Edge(Vertex aVertex1, Vertex aVertex2)
        {
            vertex1 = aVertex1;
            vertex2 = aVertex2;
        }

        public Edge(Vector3 aPosition1, Vector3 aPosition2)
        {
            vertex1 = new Vertex(aPosition1);
            vertex2 = new Vertex(aPosition2);
        }

        public Vector2 GetVertex2D(Vertex v)
        {
            return new Vector2(v.position.x, v.position.z);
        }

        public void FlipEdge()
        {
            Vertex temp = vertex1;

            vertex1 = vertex2;
            vertex2 = temp;
        }
    }
}
