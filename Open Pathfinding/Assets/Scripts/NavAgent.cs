﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace OpenPathfinding
{
    public class NavAgent : MonoBehaviour
    {
        LinkedList<Line> currentPath = new LinkedList<Line>();

        public Line CurrentTarget {
            get {
                if(currentPath == null || currentPath.Count == 0)
                {
                    return null;
                }

                return currentPath.First.Value;
            }
        }

        public void UpdateTarget()
        {
            currentPath.RemoveFirst();
        }

        public void FindPathTo(Vector3 aPoint)
        {
            Graph.instance.RequestPath(transform.position, aPoint, OnPathComplete);
        }

        private void OnPathComplete(LinkedList<Line> aPath)
        {
            currentPath = aPath;
        }

        private static void PathfindingThread(out LinkedList<Line> currentPath, Vector3 aStartPosition, Vector3 aEndPosition)
        {
            currentPath = Graph.instance.FindPathHeuristic(aStartPosition, aEndPosition);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            if (currentPath.Count > 0)
            {
                Vector3 offset = new Vector3(0, .5f, 0);
                LinkedListNode<Line> curr = currentPath.First;

                Gizmos.DrawLine(transform.position, (curr.Value.pointA + curr.Value.pointB) / 2 + offset);

                while (curr.Next != null)
                {
                    Gizmos.DrawLine((curr.Value.pointA + curr.Value.pointB) / 2 + offset, (curr.Next.Value.pointA + curr.Next.Value.pointB) / 2 + offset);
                    curr = curr.Next;
                }
            }
        }
    }
}