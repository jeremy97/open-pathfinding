﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public static class MathUtil
    {
        /// <summary>
        /// Calculates the angle between two vectors.
        /// </summary>
        /// <param name="aLhs">Left hand side</param>
        /// <param name="aRhs">Right hand side</param>
        /// <returns>The angle in degrees between the two given vectors</returns>
        public static float AngleBetween(Vector3 aLhs, Vector3 aRhs)
        {
            //Find angle using both definitions of dot product
            float angleRadians = Mathf.Acos((aLhs.x * aRhs.x + aLhs.y * aRhs.y + aLhs.z * aRhs.z) / (aLhs.magnitude * aRhs.magnitude));
            return Mathf.Rad2Deg * angleRadians;
        }

        /// <summary>
        /// Calculates the signed angle between two vectors.
        /// </summary>
        /// <param name="aLhs">Left hand side</param>
        /// <param name="aRhs">Right hand side</param>
        /// <param name="aNormal">The normalized axis of rotation, the normal of the plane the two vectors lie on</param>
        /// <returns>The signed (+/-) angle in degrees between the two given vectors</returns>
        public static float SignedAngleBetween(Vector3 aLhs, Vector3 aRhs, Vector3 aNormal)
        {
            float dot = aLhs.x * aRhs.x + aLhs.y * aRhs.y + aLhs.z * aRhs.z;
            float determinant = aLhs.x * aRhs.y * aNormal.z + aRhs.x * aNormal.y * aLhs.z + aNormal.x * aLhs.y * aRhs.z
                - aLhs.z * aRhs.y * aNormal.x - aRhs.z * aNormal.y * aLhs.x - aNormal.z * aLhs.y * aRhs.x;

            float angle = Mathf.Atan2(determinant, dot) * Mathf.Rad2Deg;
            return angle;
        }
    }
}