﻿using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    //Vertices are sort of like HalfEdges in that they overlap with the data of other polygons. Could be called HalfVertex or something.
    //Only HalfEdges serve as our actual links to other polygons. Having them link across the Vertex too would be superfluous and unnecessary.
    public class Vertex
    {
        public Vector3 position;

        //The outgoing HalfEdge (starts at this vertex)
        public HalfEdge halfEdge;

        //The Polygon this vertex is a part of
        public Polygon polygon;

        public Vertex(Vector3 aPosition)
        {
            position = aPosition;
        }

        public Vector2 GetPositionXZ()
        {
            Vector2 posXZ = new Vector2(position.x, position.z);
            return posXZ;
        }
    }
}