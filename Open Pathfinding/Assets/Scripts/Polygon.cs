﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenPathfinding
{
    public class Polygon
    {
        public List<Vertex> vertices;
        //Only need one HalfEdge to have access to all HalfEdges that make up this poly
        public HalfEdge halfEdge;

        public Polygon(List<Vertex> aVertices)
        {
            vertices = aVertices;
        }

        public Polygon(List<Vector3> aPositions)
        {
            vertices = new List<Vertex>(aPositions.Count);
            foreach(Vector3 v in aPositions)
            {
                vertices.Add(new Vertex(v));
            }
        }

        public Polygon(HalfEdge aHalfEdge)
        {
            halfEdge = aHalfEdge;
        }
    }
}