﻿using UnityEngine;

namespace OpenPathfinding
{
    public class Line
    {
        public Vector3 pointA, pointB;

        public Vector3 midpoint {
            get {
                return (pointA + pointB) / 2;
            }
        }

        public Line(Vector3 aPointA, Vector3 aPointB)
        {
            pointA = aPointA;
            pointB = aPointB;
        }
    }
}