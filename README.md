# Open Pathfinding  
  
Open Pathfinding is a library built for Unity 3D to generate nav meshes and navigate them using extensible pathfinding systems. 

Currently, this project is being developed on Unity 2018.3.0f2.

**This library is still a work in progress and should not be used in production as-is.**
  
### Dependencies  
  
- [Clipper](http://www.angusj.com/delphi/clipper.php)
- [Triangle.NET](https://archive.codeplex.com/?p=triangle)


## Example Images

#### Step 0: Many separate planes tagged as Walkable

![Step 0](https://i.imgur.com/p5IEDHk.jpg)

#### Step 1: Meshes combined using Clipper

![Step 1](https://i.imgur.com/gERnm3H.png)

#### Step 2: Clipper paths triangulated using Triangle.NET

![Step 2](https://i.imgur.com/uSRtqEd.png)

#### Step 3: Triangulated meshes convex decomposed using Hertel-Mehlhorn algorithm

![Step 3](https://i.imgur.com/tTpwXIT.jpg)

#### Step 4: 1000 agents multithreaded pathfinding!

![Step 4](https://i.imgur.com/O2v56FO.jpg)